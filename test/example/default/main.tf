
module "network"{
    source  = "../../../"
    description = "VPC deployed by terraform"
    project_id   = "optimum-archery-284619"
    network_name = "example-vpc"
    shared_vpc_host = false
}