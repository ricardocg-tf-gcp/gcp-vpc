output "network" {
  value       = module.network.network
  description = "The VPC resource being created"
}

output "network_name" {
  value       = module.network.network_name
  description = "The name of the VPC being created"
}

output "project_id" {
  value       = module.network.project_id
  description = "VPC project id"
}